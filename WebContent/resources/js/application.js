/**
 * 
 */

(function(){
	var nexus = angular.module("nexus",["ngRoute"]);
	
	nexus.config(function($routeProvider){
		$routeProvider.when("/questions/:id",{
			templateUrl : "resources/templates/questions/show.html",
			controller : "QuestionsController"
		})
		.when("/home",{
			templateUrl : "resources/templates/home.html"
		})
		.otherwise({
			redirectTo : "/home",
		})
		;
	});
})();