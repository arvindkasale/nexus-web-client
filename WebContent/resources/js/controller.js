/**
 * 
 */

(function(){

	var nexus = angular.module("nexus");
	
	var QuestionsController = function($scope, $routeParams, questionService){
		var afterQuestionLoad = function(data){
			console.log("In after question load")
			$scope.question = data;
		}
		var afterQuestionsLoad = function(data){
			$scope.questions = data;
		}
		if($routeParams.id){
			questionService.getQuestion($routeParams.id).then(function(response){afterQuestionLoad(response.data)});
			questionService.getAllQuestions().then(function(response){afterQuestionsLoad(response.data)});
		}
	}
	
	var FooterController = function($scope){
		$scope.version = "0.0.1";
		$scope.copyrightYear = "2015-2016"
	};
	var TopStoriesController = function($scope,categoryService){
		categoryService.getCategories().then(function(data){$scope.stories = data});
		$scope.trends = ["Claims of Rising Intolerance in India","European Refugee Crisis","Seventh Central Pay Commision"];
	}
	
	var SearchController = function($scope,questionService,$location){
		$scope.searchClicked = true;
		$scope.descriptionClicked = true;
		var afterSave = function(question){
			console.log(question.id);
			$location.path("/questions/"+question.id);
		}
		
		var question = questionService.getDummy();
			
		$scope.question = question;
		console.log($scope.question);
		var submitSearch = function(){
			questionService.save($scope.question).then(function(response){afterSave(response.data)});			
			console.log("Submitting form");
		}
		
		var searchFocus = function(){
			var searchDiv = $("#nexusSearch");
			searchDiv.css("width","400px");
			console.log("searh");
			$scope.searchClicked = false;
			//$("#nexusSearch").modal();
			//$scope.showModal = !$scope.showModal;
			//$("<div class='overlay fade in'></div>").insertAfter(".wrapper")
		}
		
		var showDescription = function(){
			console.log("In description");
			var flag = $(".description-clicked").hasClass("ng-hide");
			$scope.descriptionClicked = flag ? false : true;
			if(flag)
				$scope.question.description = '';
		}
		$scope.showDescription = showDescription;
		$scope.submitSearch = submitSearch;
		$scope.searchFocus = searchFocus;
	}
	nexus.controller("FooterController",FooterController);
	nexus.controller("TopStoriesController",TopStoriesController);
	nexus.controller("SearchController",SearchController);
	nexus.controller("QuestionsController",QuestionsController);
})();