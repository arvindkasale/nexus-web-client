/**
 * 
 */

(function(){

	var nexus = angular.module("nexus");
	
	var questionService = function($http){
		var save = function(question){
			console.log(question);
			return $http.post("http://localhost:8080/nexus-web-server/questions/save/",question).then(function(response){
				return response.data;
			});
		}
		
		var getQuestion = function(id){
			return $http.get("http://localhost:8080/nexus-web-server/questions/"+id).then(function(response){
				return response;
			})
		}
		var getAllQuestions = function(){
			return $http.get("http://localhost:8080/nexus-web-server/questions/").then(function(response){
				return response;
			})
		}
		
		var getDummy = function(){
			return {
				question : '',
				description : ''
			}
		}
		return{
			save : save,
			getDummy : getDummy,
			getQuestion : getQuestion,
			getAllQuestions : getAllQuestions,
		};
	};
	
	var categoryService = function($http){
						
		var getCategories = function(){
			return $http.get("http://localhost:8080/nexus-web-server/categories/").then(function(response){
				return response.data;
			});
		};
		
		return{
			getCategories : getCategories,
		};
	};	
	nexus.factory("categoryService",categoryService);	
	nexus.factory("questionService",questionService);	
})();
