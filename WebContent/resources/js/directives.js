/**
 * 
 */

(function(){
	var nexus = angular.module("nexus");
	var nexusFooter = function(){
		return{
			templateUrl : "resources/templates/layouts/footer.html"
		}
	};
	
	var nexusSearch = function(){
		return{
			templateUrl : "resources/templates/layouts/short_search.html"
		}
	}
	
	var nexusTopStories = function(){
		return{
			templateUrl : "resources/templates/questions/top_stories_categories.html"
		}
	}
	
	var mymodal =  function () {
	    return {
	      template: '<div class="modal fade">' + 
	          '<div class="modal-dialog">' + 
	            '<div class="modal-content">' + 
	              '<div class="modal-header">' + 
	                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
	                '<h4 class="modal-title">{{ title }}</h4>' + 
	              '</div>' + 
	              '<div class="modal-body" ng-transclude></div>' + 
	            '</div>' + 
	          '</div>' + 
	        '</div>',
	      restrict: 'E',
	      transclude: true,
	      replace:true,
	      scope:true,
	      link: function postLink(scope, element, attrs) {
	        scope.title = attrs.title;

	        scope.$watch(attrs.visible, function(value){
	          if(value == true)
	            $(element).modal('show');
	          else
	            $(element).modal('hide');
	        });

	        $(element).on('shown.bs.modal', function(){
	          scope.$apply(function(){
	            scope.$parent[attrs.visible] = true;
	          });
	        });

	        $(element).on('hidden.bs.modal', function(){
	          scope.$apply(function(){
	            scope.$parent[attrs.visible] = false;
	          });
	        });
	      }
	    };
	  };
	nexus.directive("mymodal",mymodal);
	nexus.directive("nexusFooter",nexusFooter);
	nexus.directive("nexusSearch",nexusSearch);
	nexus.directive("nexusTopStories",nexusTopStories);
	
})();